

// 1. initialize
console.log('program starting');
const router = require('./router');
const PORT = 3000;
const HOST = "127.0.0.1"; // 0.0.0.0
// 2. operate
router.listen(
    PORT,
    HOST, 
    () => console.log(`Listening to http://${HOST}:${PORT}`)) // backticks
// 3. cleanup
console.log('program ending');